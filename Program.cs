﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calc2
{
    class Program
    {
        static double Pars(char[] mas, ref int j)
        {
            double chislo = 0;
            if (Char.IsDigit(mas[j]))
            {
                string ch = char.ToString(mas[j]);
                j++;
                for (; j < mas.Length; j++)
                {
                    if (Char.IsDigit(mas[j]) || mas[j] == ',')
                    {
                        ch = ch + mas[j];
                    }
                    else break;

                }
                chislo = Double.Parse(ch);
                j--;
            }
            else if (Char.IsLetter(mas[j]))
            {
                string bykvy = char.ToString(mas[j]);
                j++;
                for (; j < mas.Length; j++)
                {
                    if (Char.IsLetterOrDigit(mas[j]))
                    {
                        bykvy = bykvy + mas[j];
                    }
                    else break;

                }
                Console.Write("vvedite peremennyu {0}: ", bykvy);
                chislo = double.Parse(Console.ReadLine());
                j--;
            }
            else switch (mas[j])
                {
                    case '(': j++; chislo = Calc(mas, ref j);  break;
                    case ' ': j++; chislo = Pars(mas, ref j);  break;
                }
            return chislo;
        }

        static double Calc(char[] mas, ref int i)
        {
            double result = 0;
            bool flag = true;
            for (; i < mas.Length && flag; i++)
            {
                    if (Char.IsLetterOrDigit(mas[i]))
                    {
                        result = Pars(mas, ref i);
                    }
                    else switch (mas[i])
                        {
                            case '*': i++; result = result * Pars(mas, ref i); break;
                            case '/': i++; result = result / Pars(mas, ref i); break;
                            case '+': i++; result = result + Calc(mas, ref i); if (mas[i] == ')') { i--; }; break;
                            case '-': i++; result = result - Calc(mas, ref i); if (mas[i] == ')') { i--; }; break;
                            case '(': i++; result = Calc(mas, ref i); break;
                            case ')': flag = false; break;
                        }

            }
            i--;
            return result;
        }

        static void Main(string[] args)
        {
            //Console.Write("Vvedite vyrazhenie: ");
            //string stroka = Console.ReadLine();
            //char[] mas = stroka.ToCharArray();

            //string stroka = "per+ (4,5 + 577*6*700/7,5 + 5*6)/(655*7*5/10)-3 = ";
            string stroka = "(1+2)*3/4";

            char[] str = stroka.ToCharArray();
            foreach (char el in str)
            {
                Console.Write(el);
            }
            Console.WriteLine();

            int t = 0;
            double otvet = Calc(str, ref t);
            Console.WriteLine("Otvet: {0}", otvet);
            Console.ReadKey();
        }
    }
}
